<?php
// ==============================================================================
//	Add Black Color in Mobile Browser 
// ==============================================================================

add_action( 'wp_head', 'athflex_add_color_mobile_browser' );
function athflex_add_color_mobile_browser() { ?>
    <!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#000000">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#000000">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#000000">
  <?php
}